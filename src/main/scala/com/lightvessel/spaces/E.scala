package com.lightvessel.spaces

trait Positionable{
    def has(l:Double):Boolean
    def +(p:Positionable):Positionable
    def ^(p:Positionable):Positionable
}

case class Expression(exp:(Double)=>(Boolean) = AllValues.exp) extends Positionable {
    override def has(l: Double): Boolean = exp(l)
    override def ^(p: Positionable): Positionable = p match {
        case po@PointComponent(l) if has(l) => po
        case Expression(f) => Expression((n)=> f(n) && exp(n))
    }
    override def +(p: Positionable): Positionable = p match {
        case PointComponent(l) => Expression((num) => exp(num-l))
        case _ => NoPosition //No estoy seguro de cómo tratar este caso...
    }
}

case class PointComponent(pos:Double ) extends Positionable{
    override def has(l: Double): Boolean = l == pos
    override def ^(p: Positionable): Positionable = p match {
        case PointComponent(num) if num == pos => this
        case e:Expression => e ^ this
        case _ => NoPosition
    }
    override def +(p: Positionable): Positionable = p match {
        case PointComponent(num) => PointComponent(num+pos)
        case e:Expression => e + this
        case _ => NoPosition
    }
}

object NoPosition extends Positionable{
    override def has(l: Double): Boolean = false
    override def +(p:Positionable) = NoPosition
    override def ^(p:Positionable) = NoPosition
}

object AllValues extends Expression(exp=_=>true)

object EmptySpace extends E(Map.empty[Symbol, Positionable].withDefault((_)=>NoPosition)) {
    override def ^(e:E) = EmptySpace
    override def +(e:E) = EmptySpace
}

case class E(dim:Map[Symbol, Positionable]) {
    def contains ( pairs:(Symbol, PointComponent)*) = pairs.forall( (pair) => dim(pair._1).has(pair._2.pos))
    def toAll(e:E, f:(Positionable,Positionable) => Positionable) = (this.dim.keys.toSet ++ e.dim.keys.toSet).map( k => k -> f(e.dim(k) , this.dim(k)))
    def ^(e:E) = E(toAll(e, _^_).toSeq:_*)
    def +(e:E) = E(toAll(e, _+_).toSeq:_*)
    def apply(s:Symbol) = dim(s)
}

object E {
    def apply(p:(Symbol, Positionable)*):E =
        if(Map(p:_*).exists(_._2==NoPosition)) EmptySpace else E(Map(p:_*).withDefault((key: Symbol) => AllValues))

    implicit class MapSymbol(s:Symbol){
        def ~>( n:Double):(Symbol, PointComponent) = s -> PointComponent(n)
        def ~>( e: (Double) => Boolean):(Symbol, Positionable) = s -> Expression(e)
    }

    implicit def toupleToSpace( touple: (Symbol, Double)): E = E(touple._1 ~> touple._2)
}
