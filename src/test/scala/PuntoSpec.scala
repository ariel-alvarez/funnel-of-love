import com.lightvessel.spaces.{PointComponent, EmptySpace, E}
import com.lightvessel.spaces.E.{MapSymbol, toupleToSpace}
import org.scalatest._


class PuntoSpec extends FlatSpec with BeforeAndAfter with Matchers{

    before {}

    "Un espacio" should "saber intersectarse con otro disjunto" in {
      ( 'x, 3.0 ) ^ E( 'x ~> 4, 'y ~> 1 ) shouldBe EmptySpace
    }

    it should "saber intersectarse con otro no disjunto" in {
      val interseccion = ( 'x, 3.0) ^ ( 'y , 1.0 )
      interseccion('x) shouldBe PointComponent(3.0)
      interseccion('y) shouldBe PointComponent(1.0)
    }

    it should "saber sumarse a otro" in {
      val suma = E( 'y ~> 3) + E( 'y ~> 1 )
      suma.contains('y ~> 4) shouldBe true
      suma.contains('y ~> 3) shouldBe false
    }

    it should "manejar expresiones" in {
        E( 'y ~> ( _ > 5) ) contains ('y ~> 6) shouldBe true
        E( 'y ~> ( _ > 5) ) contains ('y ~> 4) shouldBe false
        (E( 'y ~> ( _ > 5) ) ^ E( 'y ~> ( _ < 10) )) contains('y ~> 7) shouldBe true
        (E( 'y ~> ( _ > 5) ) ^ E( 'y ~> ( _ < 10) )) contains('y ~> 11) shouldBe false
    }

    it should "saber operar con expresiones" in {
        (E( 'y ~> ( _ > 5) ) + E('y ~> 2)) contains('y ~> 6) shouldBe false
        (E( 'y ~> ( _ > 5) ) + E('y ~> 2)) contains('y ~> 10) shouldBe true
    }

    it should "ser el origen de coordenadas si se intersectan los planos XY ^ XZ ^ YZ" in {
        ('x, 0.0) ^ ('y, 0.0) ^ ('z, 0.0) contains ('x ~> 0,'y ~> 0,'z ~> 0)
    }

    it should "poder ser un rectángulo si se define con rangos" in {
        val rectángulo = E( 'y ~> ((y) => y >2 && y < 10)  ) ^ E( 'x ~> ((x) => x >3 && x < 6)  )
        rectángulo contains ('x ~> 4,'y ~> 5) shouldBe true
        rectángulo contains ('x ~> 2,'y ~> 5) shouldBe false
     }


    after {}

}
